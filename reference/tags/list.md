# `<list>`

### SYNTAX:



<list is-local="..."

      cls="..."

>

…

</list>



CONTAINED IN:


MUST CONTAIN:


CAN CONTAIN:


<host>

<modifier>

<layout>

<animations>

<actions>

<path>

<response>

<data-map>

<cells>



DESCRIPTION:


One of the built-in Page tags. The page defined within this will be linked to a com.circuitry.android.coreux.ListActivity


ATTRIBUTES:


name


The unique name of the spec, used to reference the spec by name in code.

