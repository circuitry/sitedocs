Get Up and Running in 10 Minutes

Start with an empty android project. During the tutorial you should replace YOUR_PACKAGE with the appropriate value ( see your app/build.gradle file for the application id ).

Add dependencies

…


repositories {

   jcenter()

   maven { url 'http://repo.bigshipsailing.com' }

}

…


def supportLibraryVersion = "25.3.0"


dependencies {

       compile "com.android.support:appcompat-v7:${supportLibraryVersion}"

       compile "com.android.support:design:${supportLibraryVersion}"

       compile 'com.android.volley:volley:1.0.0'

       compile "com.circuitry.android:core:0.9.376"

}


Create the spec file

Create a spec file in the res/xml folder. In this example, let’s use locations.xml. The root element must be <spec>.


Add a ContentProvider

Create a ContentProvider that extends com.circuitry.android.browse.BrowseContentProvider and add it to your manifest.

   


<provider

            android:name="YOUR_PACKAGE.LocationsContentProvider"

            android:authorities="YOUR_PACKAGE.locations"

            android:enabled="true"

            android:exported="false">

            <meta-data

                android:name="com.circuitry.spec_res_id"

                android:resource="@xml/locations" />

            <meta-data

                android:name="circuitry.request.executor"

                android:value="com.circuitry.android.volley.SimpleVolleyRequestExecutor" />

        </provider>


Add an Activity

Add an Activity, e.g. MainActivity, and let it extend com.circuitry.android.coreux

ListActivity.



Now update your AndroidManifest.xml to link the MainActivity to the LocationsContentProvider

We’ll use custom meta-data entries to configure the ListActivity

 

<activity

            android:name=".MainActivity">

            <intent-filter>

                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />

            </intent-filter>

            <meta-data

                android:name="com.circuitry.uri"

                android:value="content://YOUR_PACKAGE.locations" />

            <meta-data

                android:name="com.circuitry.spec_res_id"

                android:resource="@xml/locations" />

            <meta-data

                android:name="com.circuitry.search_enabled"

                android:value="true" />

            <meta-data

                android:name="com.circuitry.bind_with_activity"

                android:value="true" />

        </activity>




com.circuitry.uri

used to connect the Activity’s LoaderManager to the LocationsContentProvider

com.circuitry.spec_res_id 

links the xml spec file to the Activity, the link is need to do bindings

com.circuitry.search_enabled

enables searching of the data

com.circuitry.bind_with_activity

ensures that the layout will bind from the root view of the activity






To use ListActivity we also need to add an attribute to our App theme. It is the color of the status view that the ListActivity shows when loading the content and reporting errors.

 

<item name="statusViewBackground">@android:color/transparent</item>



We extend ListActivity because it has all the boilerplate code to connect to the ContentProvider and bind our data. We could have used any Activity and set up a ListDelegate ourselves.




Update the specification
 

 <?xml version="1.0" encoding="utf-8"?>

<spec>

<name>locations</name>

<host>https://api.crubclub.com/1.0</host>

<list>

   <path>/locations</path>

   <response>

       <array>data</array>

   </response>

   <cells>

       <cell id="view_event_details" layout="@layout/list_cell_generic_1">

           <view id="title" name="label"/>

       </cell>

   </cells>

</list>

</spec>



We declared that the list will fetch content from <host>+<path> (https://api.crubclub.com/1.0/locations) and use the items in the array with key data to populate the list. Each item will used the same <cell> which uses the @layout/list_cell_generic_1 layout. Within that layout, the title subview ( <view id=”title”.../> ) is bound to the field with key label ( <view name=”label”.../> ) that is returned from the data source. For more information on each tag see the Glossary.


Using an extension

There are all kinds of extensions which helps to speed up development. For now let’s add the Google Maps extension.


Get your google-services.json from the firebase console and add it to your project.


Add the google plugin


app/build.gradle

 

apply plugin: 'com.google.gms.google-services'



build.gradle


classpath 'com.google.gms:google-services:3.0.0'



Add the dependency


def googleServicesVersion = "10.2.1"


dependencies {

...

       compile "com.google.android.gms:play-services-location:${googleServicesVersion}"

       compile "com.google.android.gms:play-services-maps:${googleServicesVersion}"


       compile "com.circuitry.android:maps:0.9.376"

}



Update Manifest

 

<meta-data

   android:name="com.google.android.geo.API_KEY"

   android:value="YOUR_API_KEY" />


Change MainActivity to extend from com.circuitry.extensions.maps.MapsActivity


Override MapsActivity.showList(View) such that it is a blank function, i.e. do not call super.showList(View)


Check out the samples page for more examples.