### Setup

* Create a firebase account
* Create service account
    Sign into Firebase console
    Select project
    Go to Settings
    Click Service Account menu item
* Add the Generated SSH Key to the Git repo containing the product specs

* Add your domain name to firebase auth url
```This domain (www.example.com) is not authorized to run this operation. Add it to the OAuth redirect domains list in the Firebase console -> Auth section -> Sign in method tab.  Dismiss```
#### Initial Firebase Rule
```
{
  "rules": {
    "staging": {
      "ccms": {
        "properties": {
          ".read": "auth != null",
          ".write": "auth != null"
        },
        "keypair": {
          ".read": "auth != null",
          ".write": "auth != null"
        },
        "roles": {
          ".read": "auth != null",
          ".write": "auth != null"
        },
        "users": {
          ".read": "auth != null",
          ".write": "auth != null",
          ".indexOn": [
            "email"
          ]
        },
        "themes": {
          ".read": "auth == null",
          ".write": "auth != null",
          ".indexOn": [
            "name"
          ]
        }
      },
      "form_data": {
        ".read": "auth != null",
        ".write": "auth == null",
        ".indexOn": [
          "when"
        ]
      },
      "forms": {
        ".read": "auth == null",
        ".write": "auth != null",
        ".indexOn": [
          "when"
        ]
      },
      "filters": {
        ".read": "auth == null",
        ".write": "auth != null"
      },
      "devices": {
        ".read": "auth != null",
        ".write": "auth == null"
      },
      "notifications": {
        ".read": "auth != null",
        ".write": "auth != null"
      }
    }
  }
}
```

### Notes on ElasticBeansalk

* Ensure you use NginX

* you should create an application with an Application Load Balancer
It must have at least 2 subnets ( in difference availability zones) for the ELB subnets
The VPC should have an Internet Gateway and Nat Gateway setup


### Setting up ElasticBeansalk with WebSocket via Application Load Balancer
eb init
eb create kpi-env --elb-type application --vpc

Remember to update the load balancer and it's security group to accept HTTPS


Remember to add your domain to Firebase auth URLs

see http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/customize-containers-ec2.html#linux-commands
for customizing deployment

Access Log format
type timestamp elb client:port target:port request_processing_time target_processing_time response_processing_time elb_status_code target_status_code received_bytes sent_bytes "request" "user_agent" ssl_cipher ssl_protocol target_group_arn trace_id domain_name chosen_cert_arn

https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-access-logs.html#access-log-entry-format