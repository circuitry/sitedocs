![Layers](https://docs.google.com/drawings/d/sL3O5mAh7vzLlJwyQ4wuHdA/image?parent=1YIDoEj0bo-UDhNhYr1mmOyUoKU4W3LoonoAJpgNE70o&rev=591&h=396&w=634&ac=1)

The main motivators behind this framework is ensuring parity across products and reducing development time on client products. The drawbacks are 1) the app specification is loaded at runtime and  2) the cost of developing and maintaining development tools to help create the app specification.

![Specifications](https://docs.google.com/drawings/d/s-pHXvQBHmY0t5YeZp5HvtA/image?parent=1YIDoEj0bo-UDhNhYr1mmOyUoKU4W3LoonoAJpgNE70o&rev=269&h=249&w=565&ac=1)

![Groups](https://docs.google.com/drawings/d/e/2PACX-1vSqSSBGhBo1nfsaL4KKY5xAWL7JJubPzqvW1CPrY10p3xvmFsdKR_UPDFLzMfxyZboSt0U5BlGlrEhY/pub?w=565)

# Key Components

